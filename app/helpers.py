# Helper functions that don't fit in a specific module
ALLOWED_EXTENSIONS = set(['png', 'xlsx'])


def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
