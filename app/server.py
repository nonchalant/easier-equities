from flask import Flask
from flask.ext.restful import Api

from app.api.uploads import UploadSpreadsheet
from app.views import Home

API_VERSION_NUMBER = '1.0'
API_VERSION_LABEL = 'v1'


app = Flask(__name__)
api = Api(app)

# register the Api resource routing
api.add_resource(UploadSpreadsheet, '/upload', endpoint='upload')
api.add_resource(Home, '/', endpoint='home')

# config
app.config['PROPAGATE_EXCEPTIONS'] = False
app.config['UPLOAD_FOLDER'] = 'data'


if __name__ == "__main__":
    app.run(debug=True)
