from flask import render_template, make_response
from flask.ext.restful import Resource


class Home(Resource):
    def get(self):
        """Render HTML response"""
        return make_response(render_template("index.html"))
