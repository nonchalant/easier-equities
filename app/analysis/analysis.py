import pandas as pd

from app.analysis.parsing import (
    parse_trades_df,
    serialise,
    parse_net_trades_df,
    parse_fees_returns_df,
)
from app.analysis.plots import (
    plot_trade_profit,
    plot_fees_returns_summary,
    plot_fees_returns_detailed,
    plot_fees_returns_top_ten,
    plot_all_trading_data)


def generate_results(filepath):
    df = pd.read_excel(filepath)
    trades_df = parse_trades_df(df)
    net_trades_df = parse_net_trades_df(trades_df)
    fees_returns_df, top_ten = parse_fees_returns_df(df)

    results = {
        "data": {
            "number_different_shares_owned": len(trades_df["Unit name"].unique()),
            "net_income_or_expenditure_on_equities": float(
                f"{trades_df['Debit/Credit'].sum():0.2f}"
            ),
            "number_of_buy_sell_transactions": int(trades_df.count()[0]),
            "trades_history": serialise(trades_df),
            "net_total_each_share": get_net_value_each_share(net_trades_df),
            "fees_returns_df": serialise(fees_returns_df),
        },
        "plots": {
            "trades_by_total": plot_all_trading_data(
                trades_df,
                y_category="Transaction total",
                title="Transaction amount (R)",
                size_by="Unit quantity",
                size_legend="Unit quantity (units)"
            ),
            "trades_by_unit_price": plot_all_trading_data(
                trades_df, y_category="Unit price", title="Unit price (R)", size_by="Transaction total", size_legend="Transaction total (R)"
            ),
            "trades_profit": plot_trade_profit(net_trades_df),
            "fees_returns_summary": plot_fees_returns_summary(fees_returns_df),
            "fees_returns_detailed": plot_fees_returns_detailed(fees_returns_df),
            "fees_returns_top_ten": plot_fees_returns_top_ten(top_ten),
        },
    }

    return results


def get_net_value_each_share(net_profit_df):
    """Returns net value and units of each share owned"""
    share_tally = []

    for val, df in net_profit_df.groupby("Unit name"):
        try:
            bought = df[df["Transaction type"] == "Bought"].iloc[0]["Transaction total"]
        except IndexError:
            bought = 0
        try:
            sold = df[df["Transaction type"] == "Sold"].iloc[0]["Transaction total"]
        except IndexError:
            sold = 0

        try:
            units_bought = df[df["Transaction type"] == "Bought"].iloc[0][
                "Unit quantity"
            ]
        except IndexError:
            units_bought = 0
        try:
            units_sold = df[df["Transaction type"] == "Sold"].iloc[0]["Unit quantity"]
        except IndexError:
            units_sold = 0

        total = bought - sold
        units_total = units_bought - units_sold
        share_tally.append({"name": val, "total": total, "units": units_total})

    return share_tally
