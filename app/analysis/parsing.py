import re

import pandas as pd

BUY_TERMS = ["Bought", "bought"]
SELL_TERMS = ["Sold", "sold"]
FEES_TERMS = [
    "Tax",
    "TAX",
    "tax",
    "VAT",
    "vat",
    "Vat",
    "administration",
    "Administration",
    "Commission",
    "commission",
    "Fee",
    "fee",
    "Fees",
    "fees",
]
DIVIDENDS_TERMS = [
    "Dividends",
    "Dividend",  # NB: there are negative tax entries that also match; filter out first!
    "Limited-Dividends",
    "Interest Earned",
    "Interest earned",
    "interest earned",
]


def get_transaction_type(value):
    """
    'Sold BHP Group Plc 0.6526 @ 35,212.00' --> 'Sold'
    """
    if any(substring in value.Comment for substring in BUY_TERMS):
        return "Bought"
    elif any(substring in value.Comment for substring in SELL_TERMS):
        return "Sold"
    else:
        return "Unknown"


def get_unit_name(value):
    """
    'Sold BHP Group Plc 0.6526 @ 35,212.00' --> 'BHP Group Plc'
    """
    return " ".join(value.Comment.split("@")[0].split()[1:-1])


def get_unit_price(value):
    """
    'Sold BHP Group Plc 0.6526 @ 35,212.00' --> 352.12
    """
    return (
        float(value.Comment.split("@")[1].split()[0].replace(",", "").split(".")[0])
        / 100
    )


def get_unit_quantity(value):
    """
    'Sold BHP Group Plc 0.6526 @ 35,212.00' --> 0.652
    """
    return float(value.Comment.split('@')[0].split()[-1].replace(',', ''))


def get_fee_type(value):
    minimal_terms = set([s.lower() for s in FEES_TERMS])
    minimal_terms = set(
        [s.lower()[:-1] if s.endswith("s") else s for s in minimal_terms]
    )
    for term in minimal_terms:
        if term in value.Comment.lower():
            return term


def get_return_type(value):
    minimal_terms = set([s.lower() for s in DIVIDENDS_TERMS])
    minimal_terms = set(
        [s.lower()[:-1] if s.endswith("s") else s for s in minimal_terms]
    )
    for term in minimal_terms:
        if term in value.Comment.lower():
            return term


def get_transaction_total(value):
    return abs(float(value["Debit/Credit"]))


def parse_trades_df(df):
    terms = [*BUY_TERMS, *SELL_TERMS]
    pattern = r"\b(?:{})\b".format("|".join(map(re.escape, terms)))

    trades_df = df[df["Comment"].str.contains(pattern)]
    trades_df["Transaction type"] = trades_df.apply(get_transaction_type, axis=1)
    trades_df["Unit name"] = trades_df.apply(get_unit_name, axis=1)
    trades_df["Unit price"] = trades_df.apply(get_unit_price, axis=1)
    trades_df["Unit quantity"] = trades_df.apply(get_unit_quantity, axis=1)
    trades_df["Transaction total"] = trades_df.apply(get_transaction_total, axis=1)

    return trades_df.sort_values("Date", ascending=False)


def serialise(trades_df):
    return trades_df.to_dict(orient="records")


def parse_net_trades_df(trades_df):
    net_trades_df = (
        trades_df.groupby(["Unit name", "Transaction type"]).sum().reset_index()
    )

    number_trades_df = (
        trades_df.groupby(["Unit name", "Transaction type"]).count().reset_index()
    )
    net_trades_df["Number of trades"] = list(number_trades_df["Transaction total"])

    return net_trades_df


def parse_fees_returns_df(df):
    """Returns a dataframe containing all non buy/sell transactions"""
    # categorize fee transactions
    fees_pattern = r"\b(?:{})\b".format("|".join(map(re.escape, FEES_TERMS)))
    fees_df = df[df["Comment"].str.contains(fees_pattern)]
    fees_df["Type"] = fees_df.apply(get_fee_type, axis=1)
    fees_df["Transaction total"] = fees_df.apply(get_transaction_total, axis=1)
    fees_df["Fee or return"] = "Fee"

    # Necessary in the event that there are no dividends
    # yet, in which case pandas will throw a KeyError
    try:
        # categorize returns transactions
        dividends_pattern = r"\b(?:{})\b".format(
            "|".join(map(re.escape, DIVIDENDS_TERMS)))
        dividends_df = df[
            (~df["Comment"].str.contains(fees_pattern))
            & (df["Comment"].str.contains(dividends_pattern))
            ]
        dividends_df["Type"] = dividends_df.apply(get_return_type, axis=1)
        dividends_df["Transaction total"] = dividends_df.apply(
            get_transaction_total, axis=1
        )
        dividends_df["Fee or return"] = "Return"
    except (KeyError, ValueError):
        dividends_df = pd.DataFrame()

    # concatenate and sort
    if dividends_df.empty:
        fees_returns_df = fees_df.copy()
    else:
        fees_returns_df = pd.concat([fees_df, dividends_df])
        fees_returns_df = fees_returns_df.sort_values("Date", ascending=False)

    # calculate top ten transactions for fees and returns
    fees_top_ten = fees_df.nlargest(10, "Transaction total")

    try:
        returns_top_ten = dividends_df.nlargest(10, "Transaction total")
    except AttributeError:
        returns_top_ten = pd.DataFrame()

    if returns_top_ten.empty:
        top_ten = fees_top_ten
    else:
        top_ten = pd.concat([fees_top_ten, returns_top_ten])

    return fees_returns_df, top_ten
