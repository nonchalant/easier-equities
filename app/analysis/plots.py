import json

import altair as alt


def plot_single_trades(df, title="", y_category="", size_by="", unit_name=None, size_legend=""):
    if not unit_name:
        unit_name = df["Unit name"].unique()[0]

    plot = (
        alt.Chart(df)
        .mark_point(size=80, filled=True)
        .encode(
            alt.X("Date:T", axis=alt.Axis(format="%Y/%m/%d", title="Date")),
            alt.Y(
                y_category,
                title=f"{unit_name}  |  {title}",
                scale=alt.Scale(zero=False),
            ),
            color="Transaction type",
            size=alt.Size(
                size_by, legend=alt.Legend(title=size_legend)
            ),
            tooltip=[
                "Transaction type",
                "Unit name",
                "Unit price",
                "Unit quantity",
                "Transaction total",
                "Date",
            ],
        )
        .properties(title=title, width=600)
        .interactive()
    )

    return plot


def plot_all_trading_data(trades_df, y_category="", title="", size_by="", size_legend=""):

    plots = []
    for unit_name in trades_df["Unit name"].unique():
        filtered_df = trades_df[trades_df["Unit name"] == unit_name]
        plot = plot_single_trades(
            filtered_df, title=title, y_category=y_category, unit_name=unit_name, size_by=size_by, size_legend=size_legend
        )
        plots.append(plot)

    plot_result = (
        alt.vconcat(*plots)
        .resolve_scale(
            y="independent", x="independent", size="independent", color="independent"
        )
        .resolve_legend(size="independent", color="independent")
    )
    plot_spec = plot_result.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec


def plot_trade_profit(net_profit_df):
    plot_spec = (
        alt.Chart(net_profit_df)
        .mark_bar(width=40)
        .encode(
            x="Unit name",
            y=alt.Y("Debit/Credit:Q", title="Net Bought/Sold"),
            color=alt.condition(
                alt.datum["Debit/Credit"] > 0,
                alt.value("#0489f4"),  # The positive color; blue
                alt.value("#ec1646"),  # The negative color; pink/red
            ),
            tooltip=["Unit name", "Debit/Credit", "Number of trades"],
        )
        .properties(title="Net total bought and sold for each equity owned", width=800)
        .interactive()
    )

    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec


def plot_fees_returns_summary(fees_returns_df):
    plot_spec = (
        alt.Chart(fees_returns_df)
        .mark_bar(width=60)
        .encode(
            x="Fee or return",
            y=alt.Y("sum(Transaction total):Q", title="Total amount (ZAR or USD)"),
            color=alt.condition(
                alt.datum["Fee or return"] == "Return",
                alt.value("#0489f4"),  # The positive color; blue
                alt.value("#ec1646"),  # The negative color; pink/red
            ),
            tooltip=["sum(Transaction total):Q", "count(Transaction total):Q"],
        )
        .properties(
            title="Total fees accrued vs returns earned in the form of dividends and "
            "interest",
            width=800,
        )
        .interactive()
    )

    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec


def plot_fees_returns_detailed(fees_returns_df):
    net_fees_returns_df = (
        fees_returns_df.groupby(["Type", "Fee or return"]).sum().reset_index()
    )
    number_transactions_df = (
        fees_returns_df.groupby(["Type", "Fee or return"]).count().reset_index()
    )
    net_fees_returns_df["Number of transactions"] = list(
        number_transactions_df["Transaction total"]
    )

    plot_spec = (
        alt.Chart(net_fees_returns_df)
        .mark_bar(width=60)
        .encode(
            x="Type",
            y=alt.Y("Debit/Credit:Q", title="Total fee/return amount (ZAR or USD)"),
            color=alt.condition(
                alt.datum["Debit/Credit"] > 0,
                alt.value("#0489f4"),  # The positive color; blue
                alt.value("#ec1646"),  # The negative color; pink/red
            ),
            tooltip=["Type", "Debit/Credit", "Number of transactions"],
        )
        .properties(
            title="Total earned/spent across each category of non-trading transactions",
            width=800,
        )
        .interactive()
    )

    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec


def plot_fees_returns_top_ten(top_10):
    plot_spec = (
        alt.Chart(top_10)
        .mark_bar(width=40)
        .encode(
            x="Comment",
            y=alt.Y("Transaction total:Q", title="Profit/Loss (ZAR or USD)"),
            color=alt.condition(
                alt.datum["Fee or return"] == "Return",
                alt.value("#0489f4"),  # The positive color; blue
                alt.value("#ec1646"),  # The negative color; pink/red
            ),
            tooltip=["Transaction total:Q", "Comment"],
        )
        .properties(
            title="Total fees accrued vs returns earned in the form of dividends and "
            "interest",
            width=800,
        )
    )

    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec
