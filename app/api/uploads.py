import os

from flask import request, jsonify, current_app
from flask.ext.restful import Resource, marshal_with
from flask_restful_swagger import swagger
from werkzeug.utils import secure_filename

from app.analysis import generate_results
from app.helpers import allowed_file, ALLOWED_EXTENSIONS
from .errors import JsonInvalidError
from .errors import JsonRequiredError
from .models import DummyResult
from .models import HelloResult


# Leaving the following two skeleton classes in case useful for future
class DummyEndpoint(Resource):
    @swagger.operation(responseClass=DummyResult.__name__, nickname="dummy")
    @marshal_with(DummyResult.resource_fields)
    def get(self):
        """Return a DummyResult object

        Lightweight response to let us confirm that the server is on-line"""
        return DummyResult()


class HelloEndpoint(Resource):
    @swagger.operation(
        responseClass=HelloResult.__name__,
        nickname="hello",
        responseMessages=[
            {"code": 400, "message": "Input required"},
            {"code": 500, "message": "JSON format not valid"},
        ],
        parameters=[
            {
                "name": "name",
                "description": "JSON-encoded name",
                "required": True,
                "allowMultiple": False,
                "dataType": "string",
                "paramType": "body",
            }
        ],
    )
    @marshal_with(HelloResult.resource_fields)
    def post(self):
        """Return a HelloResult object"""
        reqs = request.get_json()
        if not reqs:
            raise JsonRequiredError()
        try:
            reqs["name"]
            return HelloResult(name=reqs["name"])
        except KeyError:
            raise JsonInvalidError()


class UploadSpreadsheet(Resource):
    def post(self):
        """Return parsed data and results"""
        # check if the post request has the file part
        if "file" not in request.files:
            resp = jsonify({"message": "No file part in the request"})
            resp.status_code = 400
            return resp
        file = request.files["file"]
        if file.filename == "":
            resp = jsonify({"message": "No file selected for uploading"})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(current_app.config["UPLOAD_FOLDER"], filename)
            file.save(filepath)
            results = ""

            try:
                results = generate_results(filepath)
            except Exception as e:
                print(e)
                resp = jsonify(
                    {"message": "File formatting or processing error", "data": results}
                )
                resp.status_code = 400
                return resp
            else:
                resp = jsonify(
                    {"message": "File successfully uploaded", "data": results}
                )

                if not current_app.config["DEBUG"]:
                    os.remove(filepath)

                resp.status_code = 201
                return resp
        else:
            resp = jsonify(
                {
                    "message": f'Allowed file types are: .{", .".join(ALLOWED_EXTENSIONS)}'
                }
            )
            resp.status_code = 400
            return resp
