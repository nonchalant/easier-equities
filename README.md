Easier Equities
=================

Minimal web app for viewing and managing Easy Equities transactions

![screenshot](screenshot.png)

Installation and usage
-----

Clone the repo:

    git clone https://gitlab.com/nonchalant/easier-equities
    cd easier-equities

Create virtualenv or conda environment and install dependencies:

    conda create -n easier-equities python=3.7
    pip install -r requirements.txt
    pip install .  # or include -e for development

Launch the web app/RESTful server

    python runserver.py
    
You can upload one of the sample spreadsheets in `data` to test 
out the full suite of functionality offered by the application.

Deployment (requires a heroku account)
--------------------------------------

    heroku login
    heroku create equities-tracker-app  # run only on first time deploying
    git add .
    git commit -m "Commit message"
    git push https://git.heroku.com/equities-tracker-app.git master

License
-------

MIT, see LICENSE file

