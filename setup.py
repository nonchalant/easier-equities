from setuptools import setup, find_packages

setup(
    name='easier-equities',
    version='1.0.0',
    description='Management tool for Easy Equities transactions',
    license='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[],
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ]
)

